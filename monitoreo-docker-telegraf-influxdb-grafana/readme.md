Esta receta de Docker te permitirá montar InfluxDB, Grafana y el cliente de Telegraf para monitorear Docker, todo configurado atrás de Traefik y sin necesidad de exponer puertos, más que el 80 y 443, hacia Internet. 

Este docker-compose y el archivo de configuración, es complemento al siguiente artículo: https://www.cduser.com/como-instalar-y-configurar-telegraf/
